
library(fPortfolio)


#####################################################################
#optimization of markowitz with shrinkage estimator used as benchmark
######################################################################


returns <- read.csv("returnsIndBr.csv")
returns <- returns[1:5809,]



markowitz_opt <- matrix(0, nrow= 4550, ncol=8)


#####setting up a fGarch portfolio
frontierSpec <- portfolioSpec()
setType(frontierSpec) <- "MV"    #mean-variance 
setAlpha(frontierSpec) <- 0.05
setTargetReturn(frontierSpec) <- 0.000120
setEstimator(frontierSpec) <- "shrinkEstimator" ####robust estimator

for (i in 1:4550) {  

  ##getting in-sample data we used for weights estimation
  RZ<-returns[i:(1259+i),2:9]  #
  RZ <- as.timeSeries(RZ)
  
  ##estimating portfolio for optim step i
  frontier1g   <- fPortfolio::efficientPortfolio(data = RZ , spec = frontierSpec ,constraints ="LongOnly")

  markowitz_opt[i,1:8] <- fPortfolio::getWeights(frontier1g)

}


saveRDS(markowitz_opt, file = "markowitzShrinkW0pct.Rds") #saving data
